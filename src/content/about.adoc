---
title: "About"
layout: blocks
blocks:
  - block_ref: 'content'
    title: About us
---

link:/[The Good Docs Project] (TGDP) is a global community of technical writers, doc tools experts, software engineers, and UX designers who are committed to improving the quality of documentation in open source software and beyond.
We aim to educate and empower people to create high-quality documentation by providing them with templates, best practices, and tools to enhance their documentation in open source and beyond.

Our https://gitlab.com/tgdp/templates/-/tree/main[template repository] supports your documentation process by offering a variety of template packs tailored to meet the diverse needs of the open source community.
Each template pack is curated to ensure you have every document type necessary for thorough and engaging documentation, helping your projects to thrive and grow.

TGDP's members participate in working groups and foster a positive community culture, staffing a variety of teams on a volunteer basis.
This includes project steering committees, tech teams, template working groups and their product management teams, and community managers.
If you're passionate about making a difference in the world of open source documentation, we invite you to join us and help shape the future of The Good Docs Project.

== Origins

Our story began in 2019, when co-founder https://cameronshorter.blogspot.com/[Cameron Shorter] conceived of the idea of a global community for creating open source documentation templates during Google's inaugural https://opensource.googleblog.com/2019/03/introducing-season-of-docs.html[Season of Docs] program.

[quote,Cameron Shorter]
Surprisingly, the world didn't have an open, best-practices suite of writing templates.
A group of documentarians and open-source community members decided to create them, and we banded together and called ourselves The Good Docs Project.
We want to collate the collective wisdom of our communities into templates, writing instructions and background theory.

This idea attracted interest from both the software development and technical writing community, as well as from other open source organizations like https://www.writethedocs.org/[Write the Docs] and the https://www.osgeo.org/[OSGeo Foundation].
Big Tech companies such as Spotify, Uber, and Google also contributed.

We developed initial versions of our templates at a "docs fix-it" session at the Write the Docs Australia conference, where around 50 experts reviewed these early versions.
At the meeting, we also generated ideas for the first templates that were part of our initial alpha release.

== Moving Forward

In 2022, our project leaders conducted a major user research project where we polled and interviewed 33 technical writers from across the software industry and all over the world.
This data gave us insights into which content types required templates, and which templates would be in demand for most documentation projects.
Using that data, we created our initial template roadmap.

From that roadmap came our https://gitlab.com/tgdp/templates/-/releases[first major 1.0 template release] in June 2023, which included the first release of our core documentation pack.
The core documentation pack is our flagship template pack and includes the core, fundamental content types that every documentation project needs.
If you download any template pack for your project, it should be this one.

Another important milestone in becoming a standards setter in technical writing came in May 2023, when TGDP joined the link:/blog/we-are-gitlab-open-source-partners/[GitLab Partner Program].
Joining this project was a key moment for The Good Docs Project, signaling GitLab's recognition that our project had reached a notable level of growth and maturity.
GitLab selected our project for this program because it felt we were a healthy project that was serving a unique, important mission in open source software.
They see us as a project that can serve as a link:/blog/keystone-project/[model for other open source software projects].

Today we're at 75+ members and counting, and our team continues to develop and publish high-quality, peer-reviewed templates for a variety of documentation needs.
