---
name: The Good Docs Project
image: doctopus-holding-docs.png
description: Many team members contributed to posts from this author.
follow: Please see The Good Docs Project website (<a class="underline" href="https://thegooddocsproject.dev">https://thegooddocsproject.dev</a>) for more information.
web: https://thegooddocsproject.dev
email: gooddocsproject@gmail.com
social:
  github:
  gitlab:
  linkedin: company/the-good-docs-project
  medium:
  x:
  instagram:
---

This 'author' is used when too many people to name individually contribute to the article.
