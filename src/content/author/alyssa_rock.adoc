---
name: Alyssa Rock
image: alyssa-rock.jpg
description: Tech writer. OSS community manager. DocOps enthusiast.
follow: Connect with Alyssa on <a href="https://www.linkedin.com/in/alyssarock/">LinkedIn</a>.
web: https://alyssarock.pro/
email:
social:
  github: barbaricyawps
  gitlab: barbaricyawps
  linkedin: in/alyssarock
  medium:
  x: barbaricyawps
  instagram:
---

[.lead]
Alyssa Rock has been a technical writer for more than a decade with a specialty in writing for open source software.
Currently, she works as a technical writer for Cribl, the data engine for IT and security.

Her favorite hobby is to volunteer and she aspires to be a super connector for the communities she belongs to.
When she clocks out at the end of the day, she spends many evenings volunteering as a community manager for The Good Docs Project.
She loves connecting with the technical writers in that community who come from every part of the world to improve documentation in open source software and beyond.

Alyssa currently lives at the foot of the majestic mountains of Utah with her husband and kids.
When she's not volunteering, she's collecting vinyl records, checking out new restaurants, or seeing the latest indie flick with her community film club: The Citizens of Kane.