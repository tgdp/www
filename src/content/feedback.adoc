---
title: "Feedback"
layout: blocks
blocks:
  - block_ref: 'content'
    title: Feedback
---

== Feedback form for The Good Docs Project templates

Tell us how we can improve!

++++
<script type="text/javascript" data-categories="performance" src="https://www.cognitoforms.com/f/seamless.js" data-key="6tiz94CtckepmzIMR8C54Q" data-form="4"></script>


<script>
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  if (urlParams.has('template')) {
    const template = urlParams.get('template')

    Cognito.prefill(
        {"TemplateName": template}
    );
  }
</script>
++++