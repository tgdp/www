---
title: Talking Good Docs with the World Food Programme
date: 2024-08-27T16:00:00Z
lastmod: 2024-08-27T16:00:00Z
image: technical-documentation.png
author:
  - ravi_murugesan
tags:
  - the-project
  - outreach
custom_copyright:
custom_license: 
summary: "We recently gave an invited talk to the World Food Programme (WFP). Our session was focused on how to make a business case to improve documentation to support an organization's mission."
---

## What we spoke about

The main points we covered in our talk were:

* The why and how of good documentation.
* Common pitfalls in documentation.
* Elements of a one-page business case for documentation.
* How to focus on business impact in the business case.
* Responding to objections to invest in documentation.

## The audience

About 60 members of WFP staff from different countries joined this session. Most of them are project managers and knowledge information managers, and they have a stake in maintaining the internal knowledge base that is crucial for the humanitarian work of WFP's global staff. And the WFP is the world's largest humanitarian organization!

We got some excellent questions during the session. An attendee asked which was a better documentation approach in SharePoint: using a consistent document-naming convention with metadata or organizing documents into folders? We explained how it's best to start with an understanding of the users and critical user journeys, and then build a structure to address these.  

At the end of the session, we received encouraging feedback from the attendees. They gave an average usefulness rating of 4.25 out of 5 for the session. One attendee wrote:

> "Good documentation doesn't emerge organically and good documentation has to be a collaborative process is my highlight from today!"

## Slides and recording

* https://docs.google.com/presentation/d/1pK8phRKWWsz1pb3iBVIFqRO2HOrWMrFcXCHVqPUlPyM/edit#slide=id.p[Slides from our session for the WFP]
* https://youtu.be/UZFgbnpPX5o[Video recording of our session]

video::UZFgbnpPX5o[youtube]

## Reflections: Documentation beyond software

Our main learning from this session is that the work of The Good Docs Project is relevant not just for the open source community or the wider technology sector. Any organization that is fueled by knowledge and collaboration needs good documentation practices and templates.

For example, nonprofits and humanitarian organizations such as the WFP produce a vast amount of internal documentation, which is sometimes called a knowledge base. This knowledge base is essential to write project reports, develop fundraising proposals, and support the day-to-day work of the organization's staff, contractors, and volunteers.

Our interaction with the knowledge workers at the WFP has given us a few things to think about:

* What might be best practices to maintain a humanitarian organization's knowledge base?
* What kind of documentation templates might be useful for such an organization? And which of our existing templates might be a good starting point?
* How can tools and workflows within the organization be adapted to fit these best practices and documentation templates?

Let's see if any Good Docs volunteers are willing to tackle these!

---

Cameron Shorter and Ravi Murugesan spoke at this session, and Lana Novikova was on hand to answer questions.

For more information about The Good Docs Project, see https://thegooddocsproject.dev. Check out our templates in GitLab: https://gitlab.com/tgdp/templates.
