export function compare_post_count(a, b) {
  // converting to uppercase to have case-insensitive comparison
  const postcount1 = a.postcount;
  const postcount2 = b.postcount;

  let comparison = 0;

  if (postcount1 > postcount2) {
      comparison = -1;
  } else if (postcount1 < postcount2) {
      comparison = 1;
  }
  return comparison;
}

export function compare_tag_count(a, b) {
  // converting to uppercase to have case-insensitive comparison
  const tagcount1 = a.count;
  const tagcount2 = b.count;

  let comparison = 0;

  if (tagcount1 > tagcount2) {
      comparison = -1;
  } else if (tagcount1 < tagcount2) {
      comparison = 1;
  }
  return comparison;
}

export function compare_date(a, b) {
  // converting to uppercase to have case-insensitive comparison
  const date1 = new Date(a.date);
  const date2 = new Date(b.date);

  let comparison = 0;

  if (date1 < date2) {
      comparison = 1;
  } else if (date1 > date2) {
      comparison = -1;
  }
  return comparison;
}

export function compare_name(a, b) {
  // converting to uppercase to have case-insensitive comparison
  const name1 = a.name.toUpperCase();
  const name2 = b.name.toUpperCase();

  let comparison = 0;

  if (name1 > name2) {
      comparison = 1;
  } else if (name1 < name2) {
      comparison = -1;
  }
  return comparison;
}

export function compare_title(a, b) {
  // converting to uppercase to have case-insensitive comparison
  const title1 = a.title.toUpperCase();
  const title2 = b.title.toUpperCase();

  let comparison = 0;

  if (title1 > title2) {
      comparison = 1;
  } else if (title1 < title2) {
      comparison = -1;
  }
  return comparison;
}

export function count_words(s){
  s = s.replace(/(^\s*)|(\s*$)/gi,"");//exclude  start and end white-space
  s = s.replace(/[ ]{2,}/gi," ");//2 or more space to 1
  s = s.replace(/\n /,"\n"); // exclude newline with a start spacing
  return s.split(' ').filter(function(str){return str!="";}).length;
}

export function compile_tags(items) {
  let tagsData = items.map((item) => {return item.tags}).flat().reduce((cnt, cur) => (cnt[cur] = cnt[cur] + 1 || 1, cnt), {});
  let tagCount = []
  for (let tag in tagsData) {
    tagCount.push({
      'text':tag,
      'count':tagsData[tag]
    })
  }
  return tagCount
}