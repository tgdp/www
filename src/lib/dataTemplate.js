import { Gitlab } from '@gitbeaker/rest';
import { GITLAB_TOKEN } from '$env/static/private';
import { marked } from 'marked';
import { markedEmoji } from "marked-emoji";
import { compare_name } from './utils';
const api = new Gitlab({
  token: GITLAB_TOKEN,
});

//const templateTags = await api.Tags.all(39683789, { orderBy: 'updated' });
//const allTemplateFiles = await api.Repositories.allRepositoryTrees(39683789, { recursive: true, ref: templateTags[0].name });

/* let allTemplates = {
  "tag": templateTags[0].name,
  "message": templateTags[0].message,
  "template_data": new Array()
}; */

let allTemplates = {
  "tag": "main",
  "message": "main branch",
  "template_data": new Array()
};

function decodeBase64(base64) {
  const text = atob(base64);
  const length = text.length;
  const bytes = new Uint8Array(length);
  for (let i = 0; i < length; i++) {
    bytes[i] = text.charCodeAt(i);
  }
  const decoder = new TextDecoder(); // default is utf-8
  return decoder.decode(bytes);
}

async function getTemplatesIndex() {
  const index_path = "index.json"
  const uri = encodeURI(index_path)
  const index_data = await api.RepositoryFiles.show(39683789, uri, 'main');
  const index_content = JSON.parse(decodeBase64(index_data.content));
  return index_content;
}

const template_index = await getTemplatesIndex();

for (let index = 0; index < template_index.length; index++) {
  const template = template_index[index];
  //The string in the search function below should be updated to 'guide_' after the filenames are consistent in the next release of the Templates.
  if (template.files.guide) {
    const slug = template.name.replace(/\s+/g, '-').toLowerCase()
    const uri = encodeURI(`${slug}/${template.files.guide}`)
    const fileData = await api.RepositoryFiles.show(39683789, uri, 'main');
    const fileContent = decodeBase64(fileData.content)
    marked.use(
      markedEmoji({
        emojis: {
          "heart": "❤️",
          "information_source": "ℹ",
          "mega": "📣",
          "memo": "📝",
          "raised_hands": "🙌",
          "sparkles": "✨",
          "tada": "🎉",
          "triangular_flag_on_post": "🚩"
        },
        renderer: (token) => `${token.emoji}`
      }),
      {
        extensions: [
        {
          name: 'link',
          renderer(token) {
            var abs = new RegExp('^https?:\/\/', 'i');
            var up = new RegExp('^\.\.\/', 'i');
            var rel = new RegExp('^\/', 'i');
            var rel2 = new RegExp('^[a-z]', 'i');
            var anc = new RegExp('^#', 'i');
            let url = '';
            let ext = false;
            if (abs.test(token.href)) {
              url = token.href
              ext = true;
            } else if (up.test(token.href)) {
              url = `https://gitlab.com/tgdp/templates/-/tree/main/${token.href.replace("../", "")}`
              ext = true;
            } else if (rel.test(token.href)) {
              url = `https://gitlab.com/tgdp/templates/-/tree/main/${token.href.replace("/", "")}`
              ext = true;
            } else if (rel2.test(token.href)) {
              url = `https://gitlab.com/tgdp/templates/-/tree/main/${slug}/${token.href}`
              ext = false;
            } else if (anc.test(token.href)) {
              url = `${token.href}`
              ext = false;
            }
            return `<a ${token.title ? `title=${token.title}` : ""} ${ext?"target='_blank'":""} class="anchor"  href="${url}">${token.text}</a>`
          }
        },
        {
          name: 'heading',
          renderer(token){
            var escapedText = token.text.toLowerCase().replace(/\p{P}/gu,'').replace(/[^\w]+/g,'-');
            var renderedText = marked.parseInline(token.raw.replaceAll('#',''));
            return `<h${token.depth} id="${escapedText}" class="header">${renderedText}</h${token.depth}>`
          }
        },
        {
          name: 'image',
          renderer(token){
            var abs = new RegExp('^https?:\/\/', 'i');
            var url = "";
            if (abs.test(token.href)) {
              url = token.href
            } else {
              url = `https://gitlab.com/tgdp/templates/-/raw/main/${slug}/${token.href}?inline=false`
            }
            return `<img src="${url}" alt="${token.text}" title="${token.text}" />`
          }
        }]
      }
    );
    const html = marked.parse(fileContent)
    allTemplates.template_data.push(
      {
        slug,
        ...template,
        html
      }
    )
  }
}

allTemplates.template_data = allTemplates.template_data.sort(compare_name)

export const templates = allTemplates