import { error } from '@sveltejs/kit'
import { authors } from '$lib/dataAuthor'

export const load = (async ({ params }) => {
  const author_data = authors.find((a) => a.slug == params.slug)

  if (author_data) {
    return {
      post: author_data,
      slug: params.slug
    }
  } else {
    error(404, 'Posts were not found using this tag.');
  }
});