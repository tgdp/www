import { authors } from "$lib/dataAuthor";

export const prerender = true;

export const load = async () => {
  return {
    authors
  }
};