import { templates } from "$lib/dataTemplate";

export const prerender = true;

export const load = async () => {
  return {
    templates
  }
};