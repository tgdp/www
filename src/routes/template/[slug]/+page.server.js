import { error } from '@sveltejs/kit'
import { templates } from '$lib/dataTemplate'

export const load = (async ({ params }) => {
  const templateData = templates.template_data.find((t) => t.slug == params.slug)

  if (templateData) {
    return {
      template: templateData
    }
  } else {
    error(404, 'Template Guide not found.');
  }
});