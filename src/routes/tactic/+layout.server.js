import { authors } from "$lib/dataAuthor";
import { tactics } from "$lib/dataTactic";

export const prerender = true;

export const load = async () => {
  return {
    authors,
    tactics
  }
};