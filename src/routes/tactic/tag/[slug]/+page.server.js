import { error } from '@sveltejs/kit'
import { tactics } from '$lib/dataTactic'

export const load = (async ({ params }) => {
  const articleData = tactics.articles.filter((post)=> post.tags.includes(params.slug))

  if (articleData.length > 0) {
    return {
      articles: articleData,
      tag: params.slug
    }
  } else {
    error(404, 'Tactics were not found using this tag.');
  }
});