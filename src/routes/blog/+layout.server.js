import { authors } from "$lib/dataAuthor";
import { blogs } from "$lib/dataBlog";

export const prerender = true;

export const load = async () => {
  return {
    authors,
    blogs
  }
};