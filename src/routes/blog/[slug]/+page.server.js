import { error } from '@sveltejs/kit'
import { blogs } from '$lib/dataBlog'

export const load = (async ({params}) => {
  const blog_data = blogs.posts.find((post)=>post.slug == params.slug)

  if (blog_data) {
    return {
      post: blog_data,
      slug: params.slug
    }
  } else {
    error(404, 'Blog Post not found.');
  }
});